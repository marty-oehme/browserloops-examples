import 'package:browserloop/game/Game.dart';

abstract class LoopExample {
  Game game;

  void stop();

  void start();
}

abstract class VariableUpdates {
  void setUpdates(double updateRate);
}
